Minimac3 is a lower memory and more computationally efficient
implementation of the genotype imputation algorithms in 
minimac and minimac2.

<<< SEE http://genome.sph.umich.edu/wiki/Minimac3 FOR DOCUMENTATION >>>

A typical Minimac3 command line for imputation is as follows

../bin/Minimac3 --refHaps refPanel.vcf \ 
                --haps targetStudy.vcf \
                --prefix testRun

Please run the Test_Run.sh files in the directory /test to see examples.


<<< SEE http://genome.sph.umich.edu/wiki/Minimac3 FOR DOCUMENTATION>>>
