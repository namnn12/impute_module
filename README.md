# Copy data
```
PATH_TO_NAS=<path/to/nas>
cp -r $PATH_TO_NAS/namnn12/impute_data/GRCh38/ ./data/
```
# Folder in this repo
`data` save hg38 genetic_map, hg38 fasta and reference data to impute. Do not change this folder.  
`input` where you put your vcf after convert from genotyping data. In running process, the program will split vcf to small files and save in this folder.  
`output` save phased and imputed vcf files.  
`src` save all code.  
`tools` save tools for phasing and imputation which need when you build docker image.  
# Build docker image
```
TAG="1.6"
IMAGE_NAME="nam_impute"
docker build -t ${IMAGE_NAME}:v${TAG} .
```
# Run
```
bash run_docker.sh -n <container_name> -r <ref_data_path> -i <input_vcf_path> -o <output_folder>
```
# Parameters
`-n` container name  
`-r` path to folder contain reference data for phasing and imputation  
`-i` path to vcf file convert from genotype data  
`-o` output folder  
# Example run
```
WDIR=`pwd`
PATH_TO_NAS=/mnt/nas_share
mkdir -p output
time bash run_docker.sh -n test -r $PATH_TO_NAS/vinhdc/ref/ -i $PATH_TO_NAS/namnn12/impute_data/input/GSAv3_B3.rename.vcf.gz -o $WDIR/output/
```