while getopts i: flag
do
    case "${flag}" in
        i) input_vcf=${OPTARG};;
    esac
done

LOG_DIR="/mnt/log/"
OUTPUT_DIR="/mnt/output/"
TEMP_OUTPUT_DIR="/mnt/temp_output/"

mkdir -p "/mnt/temp_input/"
mkdir -p $TEMP_OUTPUT_DIR

cp "/mnt/input/$input_vcf" "/mnt/temp_input/$input_vcf"
bcftools index -t "/mnt/temp_input/$input_vcf"

impute () { 
    CHR=$1
    INPUT_VCF=$2

    INPUT_FILE="/mnt/temp_input/`basename ${INPUT_VCF}`"
    DATA_DIR="/mnt/data/"
    FASTA="/mnt/data/GRCh38/Homo_sapiens_assembly38.fasta"
    TEMP_INPUT_DIR="/mnt/temp_input/"
    TEMP_OUTPUT_DIR="/mnt/temp_output/"
    REF_DIR="/mnt/ref_data/"
    LOG_DIR="/mnt/log/"

    INPUT_FILE_NAME=`basename $INPUT_FILE`
    NORM_VCF="$TEMP_INPUT_DIR""${INPUT_FILE_NAME/.vcf.gz/.${CHR}.vcf.gz}"
    # ref correct and split multialleic
    NORM_LOG_FILE="${LOG_DIR}""${INPUT_FILE_NAME/.vcf.gz/.${CHR}.norm.log}"
    bcftools norm -f $FASTA -c ws -m-any -r ${CHR} ${INPUT_FILE} -Ou 2>$NORM_LOG_FILE |\
        bcftools view -Oz -o $NORM_VCF
    bcftools index -t $NORM_VCF

    ISEC_DIR="/mnt/isec/"
    mkdir -p $ISEC_DIR
    REF_VCF=`sed 's/.m3vcf/.vcf/g' <<< "$(ls ${REF_DIR}*${CHR}.*m3vcf.gz)"`
    bcftools isec -p "$ISEC_DIR$CHR" $NORM_VCF $REF_VCF

    # phase variants that are on both GSA and ref
    # INPUT=$NORM_VCF
    bgzip -c "$ISEC_DIR$CHR/0003.vcf" > "$ISEC_DIR$CHR/0003.vcf.gz"
    tabix -p vcf "$ISEC_DIR$CHR/0003.vcf.gz"
    REF_PHASE="$ISEC_DIR$CHR/0003.vcf.gz"
    MAP="${DATA_DIR}genetic_maps.b38/${CHR}.b38.gmap.gz"
    PHASE_VCF="${TEMP_OUTPUT_DIR}`basename $NORM_VCF | sed 's/.vcf.gz/.phase/'`.vcf.gz"
    PHASE_LOG="${LOG_DIR}`basename $NORM_VCF | sed 's/.vcf.gz/.phase/'`.log"
    shapeit4.2 --input $NORM_VCF --region $CHR --map $MAP \
        --reference $REF_PHASE --output $PHASE_VCF --log ${PHASE_LOG} --seed 15 >/dev/null

    # phase variants that are on GSA but not on ref
    bgzip -c "$ISEC_DIR$CHR/0000.vcf" > "$ISEC_DIR$CHR/0000.vcf.gz"
    tabix -p vcf "$ISEC_DIR$CHR/0000.vcf.gz"
    PHASE_ONLYGSA_LOG="${LOG_DIR}`basename $NORM_VCF | sed 's/.vcf.gz/.onlyGSA.phase/'`.log"
    shapeit4.2 --input "$ISEC_DIR$CHR/0000.vcf.gz" --region $CHR --map $MAP \
        --output "$ISEC_DIR$CHR/0000.phase.vcf.gz" --log ${PHASE_ONLYGSA_LOG} --seed 15 >/dev/null

    PHASE_VCF_ALL=${PHASE_VCF/.phase/.phase.sort}
    bcftools concat $PHASE_VCF "$ISEC_DIR$CHR/0000.phase.vcf.gz" -Ou | bcftools sort -Oz -o ${PHASE_VCF_ALL}
    bcftools index -t ${PHASE_VCF_ALL}

    # #impute
    # INPUT=${PHASE_VCF_ALL}
    M3VCF=$(ls ${REF_DIR}*${CHR}.*m3vcf.gz)
    IMPUTE_VCF="${TEMP_OUTPUT_DIR}"`basename $PHASE_VCF_ALL | sed 's/phase.sort/impute/; s/.vcf.gz//'`
    IMPUTE_LOG="${LOG_DIR}"`basename $PHASE_VCF_ALL | sed 's/phase.sort/impute/; s/.vcf.gz/.log/'`
    minimac4 --haps $PHASE_VCF_ALL --refHaps $M3VCF \
        --ChunkLengthMb 20 --ChunkOverlapMb 3 --allTypedSites --ignoreDuplicates \
        --prefix $IMPUTE_VCF --log --cpus 8 >/dev/null
    cp "${IMPUTE_VCF}.logfile" ${IMPUTE_LOG}

    echo "Finish ${CHR}"
}
export -f impute

echo "Start run all chromosomes"
parallel -j 8 impute chr{} {} ::: {1..22} ::: $input_vcf
wait

echo "Concating..."
OUTPUT_VCF="${OUTPUT_DIR}${input_vcf/.vcf.gz/.ALL.vcf.gz}"
OUTPUT_LOG="${LOG_DIR}${input_vcf/.vcf.gz/.ALL.concat.log}"
bcftools concat $(ls /mnt/temp_output/*.impute.dose.vcf.gz | sort -V -s) -Oz -o $OUTPUT_VCF > ${OUTPUT_LOG} 2>&1
bcftools index -t $OUTPUT_VCF

echo "DONE ALL!!!"