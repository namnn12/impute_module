FROM ubuntu:20.04
RUN apt-get update  && apt-get install -y wget curl\
    gcc \
    make \
    build-essential \
    libbz2-dev \
    zlib1g-dev \
    libncurses5-dev  \
    libncursesw5-dev \
    liblzma-dev \
    libcurl4-openssl-dev\
    libcurl3-gnutls \
    parallel

COPY /install_tools.sh /
RUN bash /install_tools.sh
COPY /tools/Minimac3Executable/bin/Minimac3 /usr/local/bin
COPY /tools/Minimac3Executable/bin/Minimac3-omp /usr/local/bin
COPY /tools/Minimac4/build/minimac4 /usr/local/bin
COPY /tools/shapeit4-4.2.2/bin/shapeit4.2 /usr/local/bin