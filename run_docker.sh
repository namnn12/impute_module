while getopts n:r:i:o: flag
do
    case "${flag}" in
        n) container_name=${OPTARG};;
        r) ref_path=${OPTARG};;
        i) input_vcf_path=${OPTARG};;
        o) output_dir=${OPTARG};;
    esac
done

REPO_NAME=$container_name
TAG="1.6"
IMAGE_NAME="nam_impute"

WORK_DIR=`pwd`
DATA_DIR="${WORK_DIR}/data"
REF_DIR="${ref_path}"
SRC_DIR="${WORK_DIR}/src"
INPUT_FILE=`basename $input_vcf_path`
INPUT_DIR="${input_vcf_path%"`basename $input_vcf_path`"}"
OUTPUT_DIR="${output_dir}"
LOG_DIR="${WORK_DIR}/log"
mkdir -p ${LOG_DIR}

docker run --name ${REPO_NAME} \
    -v ${DATA_DIR}:/mnt/data/ \
    -v ${REF_DIR}:/mnt/ref_data/ \
    -v ${INPUT_DIR}:/mnt/input/ \
    -v ${OUTPUT_DIR}:/mnt/output/ \
    -v ${LOG_DIR}:/mnt/log/ \
    -v ${SRC_DIR}:/mnt/src/ \
    -itd --memory="32g" --cpus="8.0" ${IMAGE_NAME}:v${TAG}
echo "Docker run at ${REPO_NAME}"

docker exec -it ${REPO_NAME} bash -c "bash /mnt/src/run_impute.sh -i ${INPUT_FILE}"

echo -n "Stop "
docker stop ${REPO_NAME}
echo -n "Remove "
docker rm ${REPO_NAME}